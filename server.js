var http = require("http");
var qs = require("querystring");
var fs = require("fs")


var server = http.createServer(function (req, res) {
    // console.log(req.method) // zauważ ze przesyłane po kliknięciu butona dane, będą typu POST
    // console.log(req.url)
    switch (req.method) {
        case "GET":
            if (req.url == "/") {
                fs.readFile("static/index.html", function (error, data) {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url == "/style.css") {
                fs.readFile("static/style.css", function (error, data) {
                    res.writeHead(200, { 'Content-Type': 'text/css' });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url.indexOf(".js") != -1) {
                fs.readFile("static/" + decodeURI(req.url), function (error, data) {
                    res.writeHead(200, { "Content-type": "application/javascript" });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url.indexOf(".mp3") != -1) {
                fs.readFile("static/" + decodeURI(req.url), function (error, data) {
                    res.writeHead(200, { "Content-type": "audio/mp3" });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url.indexOf(".jpg") != -1) {
                fs.readFile("static/" + decodeURI(req.url), function (error, data) {
                    res.writeHead(200, { "Content-type": "image/jpg" });
                    res.write(data);
                    res.end();
                })
            }
            break;
        case "POST":
            servResponse(req, res)
            break;
    }
})
var playlista = [];
var servResponse = function (req, res) {
    var allData = "";
    req.on("data", function (data) {
        allData += data
    })
    req.on("end", function (data) {
        var finish = qs.parse(allData)
        var obj = {
            katalogi: [],
            pliki: [],
            okladki: []
        }
        var first = undefined
        if (finish.action == "first" || finish.action == "okladka" || finish.action == "mp3play") {
            fs.readdir("static/jpg", function (err, files) {
                if (err) {
                    return console.log(err);
                }
                files.forEach(function (file) {
                    obj.okladki.push(file)
                })
            })
            fs.readdir("static/mp3", function (err, files) {
                if (err) {
                    return console.log(err);
                }
                files.forEach(function (file) {
                    if (!first) {
                        first = file
                    }
                    obj.katalogi.push(file)
                });
                if (finish.action == "okladka") {
                    first = finish.album
                }
                fs.readdir("static/mp3/" + first, function (err, files) {
                    if (err) {
                        return console.log(err);
                    }
                    files.forEach(function (file) {
                        var stats = fs.statSync("static/mp3/" + first + "/" + file);
                        stats.size = Math.round(stats.size / 1024) + "KB"
                        var plik = { "file": file, "rozmiar": stats.size }
                        obj.pliki.push(plik)
                    });
                    // console.log(obj)
                    res.writeHead(200, { 'Content-Type': 'text/html' }); //ajax
                    res.end(JSON.stringify(obj)); //ajax
                });
            });
        }
        else if (finish.action == "mp3nextprev") { //klikanie w NEXT/PREV
            fs.readdir("static/mp3", function (err, files) {
                if (err) {
                    return console.log(err);
                }
                fs.readdir("static/mp3/" + finish.album, function (err, files) {
                    if (err) {
                        return console.log(err);
                    }
                    files.forEach(function (file) {
                        var stats = fs.statSync("static/mp3/" + finish.album + "/" + file);
                        stats.size = Math.round(stats.size / 1024) + "KB"
                        var plik = { "file": file, "rozmiar": stats.size }
                        obj.pliki.push(plik)
                    });
                    // console.log(obj)
                    res.writeHead(200, { 'Content-Type': 'text/html' }); //ajax
                    res.end(JSON.stringify(obj)); //ajax
                });
            });
        }
        else if (finish.action == "playlistAdd") { //DODAWANIE DO PLAYLISTY
            var obj1 = {
                tytul: finish.tytul,
                album: finish.album,
            }
            var repeatedTitle = false
            for (var i = 0; i < playlista.length; i++) {
                if (playlista[i].tytul == finish.tytul) {
                    repeatedTitle = true
                    break
                }
            }
            if (!repeatedTitle) playlista.push(obj1)
            // console.log(playlista)
            res.writeHead(200, { 'Content-Type': 'text/html' }); //ajax
            res.end(); //ajax
        }
        else if (finish.action == "playlistShow") { //klikanie w NEXT/PREV
            fs.readdir("static/mp3", function (err, files) {
                if (err) {
                    return console.log(err);
                }

                for (var i = 0; i < playlista.length; i++) {
                    var stats = fs.statSync("static/mp3/" + playlista[i].album + "/" + playlista[i].tytul);
                    stats.size = Math.round(stats.size / 1024) + "KB"
                    var plik = { "file": playlista[i].tytul, "rozmiar": stats.size }
                    obj.pliki.push(plik)
                    obj.katalogi.push(playlista[i].album)
                }
                // console.log(obj)
                res.writeHead(200, { 'Content-Type': 'text/html' }); //ajax
                res.end(JSON.stringify(obj)); //ajax
            });
        }
    })
}

server.listen(4000, function () {
    console.log("serwer startuje na porcie 4000")
});