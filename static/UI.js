console.log("UI.js działa")

class UI {

    constructor() { }

    albumClick(obj, okladka, albumPlaylista) { //okladka
        // console.log(obj)
        mp3.currentPage = false
        $("#lista").empty()
        var head = $("<div class='piosenka'><div class='album'>Album/Wykonawca</div><div class='tytul'>Tytuł</div><div class='rozmiar'>Rozmiar</div><div class='wypelnienie'></div><div class='wypelnienie'></div></div>")
        $("#lista").append(head)
        mp3.playlista = []
        for (var i = 0; i < obj.pliki.length; i++) {
            if (okladka) {
                mp3.lastClick = "album"
                var plikDoDefault = {
                    "id": i,
                    "album": okladka,
                    "tytul": obj.pliki[i].file,
                    "rozmiar": obj.pliki[i].rozmiar
                }
                mp3.currentList.push(plikDoDefault)
            }
            else if (albumPlaylista) {
                mp3.lastClick = "playlista"
                var plikDoPlaylisty = {
                    "id": i,
                    "album": albumPlaylista[i],
                    "tytul": obj.pliki[i].file,
                    "rozmiar": obj.pliki[i].rozmiar
                }
                mp3.playlista.push(plikDoPlaylisty)
            }
            // console.log(plikDoPlaylisty)

            if (okladka) var divv = $("<div class='piosenka' id=piosenka" + i + "><div class='album'>" + okladka + "</div><div class='tytul'>" + obj.pliki[i].file + "</div><div class='rozmiar'>" + obj.pliki[i].rozmiar + "</div><div class='play fas fa-play' id=play" + i + "></div><div class='playlista fas fa-plus' id=playlista" + i + "></div></div>")
            else if (albumPlaylista) var divv = $("<div class='piosenka' id=piosenka" + i + "><div class='album'>" + albumPlaylista[i] + "</div><div class='tytul'>" + obj.pliki[i].file + "</div><div class='rozmiar'>" + obj.pliki[i].rozmiar + "</div><div class='play fas fa-play' id=play" + i + "></div><div class='playlista fas fa-plus' id=playlista" + i + "></div></div>")

            $("#lista").append(divv)
        }
    }

    playClick() {
        $("#player1").css("visibility", "visible");
        var id = this.id.split("play")[1];
        if (mp3.lastClick == "album") mp3.lastClickPlay = "album"
        if (mp3.lastClick == "playlista") mp3.lastClickPlay = "playlista"

        if (id) { //klikniecie w play przy piosence
            ui.listaGora(id)
        }
        else { //klikniecie w play przy playerze na dole ekranu
            ui.playerDol()
        }
    }
    playlistAdd() {
        var clickedSong = $(this).parent().children(".tytul")[0].innerHTML
        var clickedAlbum = $(this).parent().children(".album")[0].innerHTML
        music.playlistAdd(clickedSong, clickedAlbum)
    }
    playlistShow() {

    }

    listaGora(id) {
        mp3.currentID = id
        mp3.currentPage = true
        if (document.getElementsByClassName("selected")[0]) {
            document.getElementsByClassName("selected")[0].classList.remove("selected")
        }

        document.getElementById("piosenka" + id).classList.add("selected")
        for (var i = 0; i < 5; i++) {
            if (document.getElementById("piosenka" + i)) {
                if (document.getElementById("piosenka" + i).getElementsByClassName("fa-pause")[0]) {
                    if (!document.getElementById("piosenka" + i).classList.contains("selected")) {
                        document.getElementById("piosenka" + i).getElementsByClassName("fa-pause")[0].classList.add("fa-play")
                        document.getElementById("piosenka" + i).getElementsByClassName("fa-pause")[0].classList.remove("fa-pause")
                    }
                }
            }
        }
        mp3.currentSong = $(".selected").children(".tytul")[0].innerHTML
        mp3.currentAlbum = $(".selected").children(".album")[0].innerHTML

        if ($(".selected").children(".fa-pause")[0]) { //gdy jest spauzowane
            $(".selected").children(".fa-pause")[0].classList.add("fa-play") //przy piosence
            $(".selected").children(".fa-pause")[0].classList.remove("fa-pause") //przy piosence
            mp3.playing = false
            music.play(mp3.playing, mp3.currentSong, mp3.currentAlbum)
            if ($("#player1").children(".fa-pause")[0]) {
                $("#player1").children(".fa-pause")[0].classList.add("fa-play") //player u dołu
                $("#player1").children(".fa-pause")[0].classList.remove("fa-pause") //player u dołu
            }
        }
        else if ($(".selected").children(".fa-play")[0]) { //gdy nie jest spauzowane
            $(".selected").children(".fa-play")[0].classList.add("fa-pause") //przy piosence
            $(".selected").children(".fa-play")[0].classList.remove("fa-play") //przy piosence
            mp3.playing = true
            music.play(mp3.playing, mp3.currentSong, mp3.currentAlbum)
            if ($("#player1").children(".fa-play")[0]) {
                $("#player1").children(".fa-play")[0].classList.add("fa-pause") //player u dołu
                $("#player1").children(".fa-play")[0].classList.remove("fa-play") //player u dołu
            }
        }
    }
    playerDol() {
        if (document.getElementsByClassName("selected")[0]) { //gdy jestes caly czas na tej samej podstronie
            if ($(".selected").children(".fa-pause")[0]) { //gdy jest spauzowane
                $(".selected").children(".fa-pause")[0].classList.add("fa-play") //przy piosence
                $(".selected").children(".fa-pause")[0].classList.remove("fa-pause") //przy piosence
                mp3.playing = false
                music.play(mp3.playing, mp3.currentSong, mp3.currentAlbum)
                if ($("#player1").children(".fa-pause")[0]) {
                    $("#player1").children(".fa-pause")[0].classList.add("fa-play") //player u dołu
                    $("#player1").children(".fa-pause")[0].classList.remove("fa-pause") //player u dołu
                }
            }
            else if ($(".selected").children(".fa-play")[0]) { //gdy nie jest spauzowane
                $(".selected").children(".fa-play")[0].classList.add("fa-pause") //przy piosence
                $(".selected").children(".fa-play")[0].classList.remove("fa-play") //przy piosence
                mp3.playing = true
                music.play(mp3.playing, mp3.currentSong, mp3.currentAlbum)
                if ($("#player1").children(".fa-play")[0]) {
                    $("#player1").children(".fa-play")[0].classList.add("fa-pause") //player u dołu
                    $("#player1").children(".fa-play")[0].classList.remove("fa-play") //player u dołu
                }
            }
        }
        else if (!document.getElementsByClassName("selected")[0]) { //po przejsciu na inna podstrone
            if ($("#player1").children(".fa-pause")[0]) {
                $("#player1").children(".fa-pause")[0].classList.add("fa-play") //player u dołu
                $("#player1").children(".fa-pause")[0].classList.remove("fa-pause") //player u dołu
                mp3.playing = false
                music.play(mp3.playing, mp3.currentSong, mp3.currentAlbum)
            }
            else if ($("#player1").children(".fa-play")[0]) {
                $("#player1").children(".fa-play")[0].classList.add("fa-pause") //player u dołu
                $("#player1").children(".fa-play")[0].classList.remove("fa-play") //player u dołu
                mp3.playing = true
                music.play(mp3.playing, mp3.currentSong, mp3.currentAlbum)
            }
        }
    }
    nextClick() {
        if (mp3.lastClickPlay == "album") music.next(mp3.currentSong, mp3.currentAlbum, mp3.currentID, "next", "album")
        else if (mp3.lastClickPlay == "playlista") music.next(null, null, mp3.currentID, "next", "playlista")
    }
    prevClick() {
        if (mp3.lastClickPlay == "album") music.next(mp3.currentSong, mp3.currentAlbum, mp3.currentID, "prev", "album")
        else if (mp3.lastClickPlay == "playlista") music.next(null, null, mp3.currentID, "prev", "playlista")
    }
    currentSongUI() {
        document.getElementById("currentSong").innerHTML = mp3.currentAlbum + "<br>" + mp3.currentSong
    }
}