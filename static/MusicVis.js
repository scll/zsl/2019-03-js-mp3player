console.log("MusicVis.js działa")

class MusicVis {

    constructor() {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        this.audioContext = new AudioContext();
        this.audioElement = document.getElementById("audio");
        this.source = this.audioContext.createMediaElementSource(this.audioElement);
        this.analyser = this.audioContext.createAnalyser();
        this.source.connect(this.analyser);
        this.analyser.connect(this.audioContext.destination);
        this.analyser.fftSize = 256;
        this.dataArray = new Uint8Array(this.analyser.frequencyBinCount);
        this.analyser.getByteFrequencyData(this.dataArray);
        console.log(this.dataArray);
        this.chrome()
    }
    getData() {
        this.analyser.getByteFrequencyData(this.dataArray);
        return this.dataArray.toString();
    }
    chrome() { //dzieki czemu klasa działa w chrome
        this.audioContext.resume().then(function () {
            console.log("audioContext lives!")
        })
    }
}