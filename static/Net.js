console.log("Net.js działa")

class Net {

    constructor() { this.handleData() }

    handleData() {
        $.ajax({
            data: { "action": "first" },
            type: "POST",
            success: function (data) {
                // console.log(data)
                var obj = JSON.parse(data)
                // console.log(obj)

                var listaPiosenekOnLoad = obj.okladki[0]
                listaPiosenekOnLoad = listaPiosenekOnLoad.substring(0, listaPiosenekOnLoad.length - 4)
                ui.albumClick(obj, listaPiosenekOnLoad);
                var okladki = obj.okladki

                okladki.forEach(function (pic) {
                    var dv = document.createElement('div')
                    var img = document.createElement("img")
                    img.alt = pic.substring(0, pic.length - 4)
                    dv.classList.add("pic1")
                    img.src = "jpg/" + pic
                    dv.appendChild(img)
                    $("#pic").append(dv)
                });
                $("img").click(function () {
                    var nazwa = this.alt;
                    net.sendData(nazwa)
                })
                $(".play").click(ui.playClick) //PLAY
                $(".playDol").click(ui.playClick)
                $(".prev").click(ui.prevClick)
                $(".next").click(ui.nextClick)
                $(".playlista").click(ui.playlistAdd)
                $("#playlistaBt1").click(net.playlistShow)
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    sendData(nazwa) { //klikniecie w okladke 
        var okladka = nazwa
        $.ajax({
            data: { "action": "okladka", "album": okladka },
            type: "POST",
            context: document.body,
            success: function (data) {
                var obj = JSON.parse(data)

                ui.albumClick(obj, okladka);

                $(".playlista").click(ui.playlistAdd)
                $(".play").click(ui.playClick)
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    playlistShow() {
        $.ajax({
            data: { "action": "playlistShow" },
            type: "POST",
            context: document.body,
            success: function (data) {
                var obj = JSON.parse(data)

                ui.albumClick(obj, null, obj.katalogi);

                $(".playlista").click(ui.playlistAdd)
                $(".play").click(ui.playClick)
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });

    }
}
