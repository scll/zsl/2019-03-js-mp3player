console.log("Music.js działa")

class Music {

    constructor() { }

    play(playing, tytul, album) { //klikniecie w play
        $.ajax({
            data: { "action": "mp3play", "album": album, "tytul": tytul },
            type: "POST",
            context: document.body,
            success: function (data) {
                var obj = JSON.parse(data)
                // console.log("kliknieta piosenka: " + tytul + ", album: " + album)
                ui.currentSongUI()
                // ui.klik(obj, okladka);
                if (playing) {
                    var splittedSrc = decodeURI($("#audio_src").prop("src").split("/mp3/")[1])
                    if (splittedSrc != album + "/" + tytul) {
                        $("#audio_src").prop("src", "mp3/" + album + "/" + tytul);
                        $("#audio").trigger("load");

                        music.load()
                    }
                    $("#audio").trigger("play");
                }
                if (!playing) $("#audio").trigger("pause");


            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    playlistAdd(tytul, album) {
        $.ajax({
            data: { "action": "playlistAdd", "album": album, "tytul": tytul },
            type: "POST",
            // success: function (data) {
            // var obj = JSON.parse(data)
            // console.log(obj)
            // },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });

    }
    next(tytul, album, id, nextprev, clickedAlbum) { //klikniecie w play
        $.ajax({
            data: { "action": "mp3nextprev", "album": album, "tytul": tytul },
            type: "POST",
            context: document.body,
            success: function (data) {
                var obj = JSON.parse(data)
                // console.log(obj)
                // ui.klik(obj, okladka);
                // console.log(album)
                // console.log(obj.pliki.length)
                if (document.getElementsByClassName("selected")[0]) {
                    document.getElementsByClassName("selected")[0].classList.remove("selected")
                }
                // if ($("#player1").children(".fa-play")[0]) {
                //     $("#player1").children(".fa-play")[0].classList.add("fa-pause") //player u dołu
                //     $("#player1").children(".fa-play")[0].classList.remove("fa-play") //player u dołu
                // }
                function selected() {
                    if (mp3.currentPage) {
                        document.getElementById("piosenka" + mp3.currentID).classList.add("selected")
                        ui.listaGora(mp3.currentID)
                    }
                }
                function updateWizualnyPlaylista(id1) {
                    mp3.currentID = id1
                    mp3.currentSong = mp3.playlista[id1].tytul
                    mp3.currentAlbum = mp3.playlista[id1].album
                    ui.currentSongUI()
                    selected()
                }
                function updateWizualnyAlbum(id2, album) {
                    mp3.currentID = id2
                    mp3.currentSong = obj.pliki[id2].file
                    mp3.currentAlbum = album
                    ui.currentSongUI()
                    selected()

                }
                if (clickedAlbum == "album") {
                    if (nextprev == "next") {
                        var idPLUS1 = parseInt(id) + 1
                        if (obj.pliki[idPLUS1]) {
                            $("#audio_src").prop("src", "mp3/" + album + "/" + obj.pliki[idPLUS1].file);
                            $("#audio").trigger("load");
                            music.load()
                            updateWizualnyAlbum(idPLUS1, album)
                        }
                        else {
                            $("#audio_src").prop("src", "mp3/" + album + "/" + obj.pliki[0].file);
                            $("#audio").trigger("load");
                            music.load()

                            if (mp3.playing) { $("#audio").trigger("play"); }
                            updateWizualnyAlbum(0, album)
                        }
                    }
                    else if (nextprev == "prev") {
                        var idMINUS1 = parseInt(id) - 1
                        if (obj.pliki[idMINUS1]) {
                            $("#audio_src").prop("src", "mp3/" + album + "/" + obj.pliki[idMINUS1].file);
                            $("#audio").trigger("load");
                            music.load()

                            if (mp3.playing) { $("#audio").trigger("play"); }
                            updateWizualnyAlbum(idMINUS1, album)
                        }
                        else {
                            $("#audio_src").prop("src", "mp3/" + album + "/" + obj.pliki[(obj.pliki.length - 1)].file);
                            $("#audio").trigger("load");
                            music.load()

                            if (mp3.playing) { $("#audio").trigger("play"); }
                            updateWizualnyAlbum((obj.pliki.length - 1), album)
                        }
                    }
                }
                else if (clickedAlbum == "playlista") {
                    if (nextprev == "next") {
                        var idPLUS1 = parseInt(id) + 1
                        if (mp3.playlista[idPLUS1]) {
                            $("#audio_src").prop("src", "mp3/" + mp3.playlista[idPLUS1].album + "/" + mp3.playlista[idPLUS1].tytul);
                            $("#audio").trigger("load");
                            music.load()

                            updateWizualnyPlaylista(idPLUS1)
                        }
                        else {
                            $("#audio_src").prop("src", "mp3/" + mp3.playlista[0].album + "/" + mp3.playlista[0].tytul);
                            $("#audio").trigger("load");
                            music.load()

                            if (mp3.playing) { $("#audio").trigger("play"); }
                            updateWizualnyPlaylista(0)
                        }
                    }
                    else if (nextprev == "prev") {
                        var idMINUS1 = parseInt(id) - 1
                        if (mp3.playlista[idMINUS1]) {
                            $("#audio_src").prop("src", "mp3/" + mp3.playlista[idMINUS1].album + "/" + mp3.playlista[idMINUS1].tytul);
                            $("#audio").trigger("load");
                            music.load()

                            if (mp3.playing) { $("#audio").trigger("play"); }
                            updateWizualnyPlaylista(idMINUS1)
                        }
                        else {
                            var idOSTATNI1 = mp3.playlista.length - 1
                            $("#audio_src").prop("src", "mp3/" + mp3.playlista[idOSTATNI1].album + "/" + mp3.playlista[idOSTATNI1].tytul);
                            $("#audio").trigger("load");
                            music.load()

                            if (mp3.playing) { $("#audio").trigger("play"); }
                            updateWizualnyPlaylista(idOSTATNI1)
                        }
                    }
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    load() {
        function leadingZero(i) {
            return (i < 10) ? '0' + i : i;
        }
        function updateWizualny() {
            document.getElementById("currentTime").style.visibility = "visible"
            var minAct = Math.floor(parseInt($("#audio").prop("currentTime")) / 60)
            var secAct = Math.floor(parseInt($("#audio").prop("currentTime")))
            var minDur = Math.floor(parseInt($("#audio").prop("duration")) / 60)
            var secDur = Math.floor(parseInt($("#audio").prop("duration")))
            document.getElementById("minCurr").innerHTML = leadingZero(minAct); //minuty
            document.getElementById("secCurr").innerHTML = leadingZero((secAct >= 60) ? secAct - 60 * minAct : secAct) //sekundy
            if (secDur) document.getElementById("minDur").innerHTML = leadingZero(minDur)//czasTrwania - minuty
            if (secDur) document.getElementById("secDur").innerHTML = leadingZero((secDur >= 60) ? secDur - 60 * minDur : secDur)//czasTrwania - sekundy
            document.getElementById("pasekCzas").style.width = ((secAct / secDur) * 100) + "%"
        }
        $("#audio").on("loadeddata", function () {
            if (mp3.playing) {
                $("#audio").trigger("play");
                $("#audio").on("timeupdate", function () {
                    updateWizualny()
                });
            }
            else if (!mp3.playing) {
                // $("#audio").trigger("play");
                $("#audio").on("timeupdate", function () {
                    updateWizualny()
                });
            }
        });
    }
}